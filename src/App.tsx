import React, { useState, useEffect, useCallback } from "react";
import { AnimatePresence, motion } from "framer-motion";
import "./App.css";
import "./main.css";

function App() {
  const [source] = useState([
    "short hop nair rising",
    "short hop bair rising",
    "short hop fair rising",
    "full hop nair",
    "full hop bair",
    "full hop fair",
    "b reverse",
    "wavebounce",
    "short hop falling nairs",
    "short hop falling bairs",
    "short hop falling fairs",
  ]);
  const getShuffledWords = useCallback(() => {
    let n = source.length - 1;
    let result = [];
    let words = JSON.parse(JSON.stringify(source));
    while (n >= 0) {
      const randomIndex = Math.floor(Math.random() * words.length);
      result.push(words[randomIndex]);
      words.splice(randomIndex, 1);
      n -= 1;
    }
    return result;
  }, [source]);

  const [words, setWords] = useState(getShuffledWords());
  const [state, setState] = useState({
    showStartMessage: true,
    showWord: false,
    word: "",
    index: 0,
  });

  useEffect(() => {
    if (state.showWord === false && state.showStartMessage === false) {
      if (state.index === words.length - 1) {
        setWords(getShuffledWords());
        setTimeout(() => {
          setState({
            ...state,
            showStartMessage: true,
            showWord: false,
            index: 0,
            word: "",
          });
        }, 300);
      } else {
        setTimeout(() => {
          setState({
            ...state,
            word: words[state.index + 1],
            index: state.index + 1,
            showWord: true,
          });
        }, 1000);
      }
    } else if (state.showWord === true) {
      setTimeout(() => {
        setState({
          ...state,
          showWord: false,
        });
      }, 2000);
    }
  }, [getShuffledWords, state, words]);

  const start_app = () => {
    setState({
      ...state,
      showStartMessage: false,
    });
  };

  const start_markup = (
    <AnimatePresence>
      {state.showStartMessage && (
        <>
          <motion.h1
            initial={true}
            animate={{ scaleX: [0.9, 1.1, 1] }}
            transition={{
              ease: "easeOut",
              duration: 0.2,
            }}
            whileHover={{ scale: 1.1 }}
            whileTap={{ scale: 0.9 }}
            exit={{ opacity: 0 }}
            className="text-6xl text-white"
          >
            Words App
          </motion.h1>
          <motion.button
            whileHover={{ scale: 1.1 }}
            whileTap={{ scale: 0.9 }}
            exit={{ opacity: 0 }}
            transition={{ opacity: { ease: "easeOut", duration: 0.2 } }}
            className=" transition-colors duration-300 ease-in-out hover:scale-125 font-semibold  py-2 px-4 hover:border-transparent rounded text-4xl my-4 start-button"
            onClick={start_app}
          >
            Start
          </motion.button>
        </>
      )}
    </AnimatePresence>
  );

  const word_markup = (
    <AnimatePresence>
      {state.showWord && (
        <motion.h1
          initial={true}
          exit={{ scale: 0, opacity: 0 }}
          animate={{ opacity: [0, 1] }}
          transition={{ duration: 0.2 }}
          className="text-white text-5xl opacity-0 text-center lowercase"
        >
          {state.word}
        </motion.h1>
      )}
    </AnimatePresence>
  );

  return (
    <div className="App flex items-center justify-center min-h-screen min-w-screen flex-col">
      {start_markup}
      {word_markup}
    </div>
  );
}

export default App;
